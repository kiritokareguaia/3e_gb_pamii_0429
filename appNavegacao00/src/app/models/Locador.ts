export class Locador {
    private _nome: string;
    private _telefone: string;
    private _dataNascimento: Date;
    private _email: string;
    private _fone: string;
    private _sexo: string;
    private _rendaMensal: number;
    private _nomeFiador: string;
    constructor(nome:string, telefone:string, dataNascimento:Date, email:string, fone:string, sexo:string, rendaMensal:number, nomeFiador:string)
    {
        this._dataNascimento=dataNascimento;
        this._email=email;
        this._fone=fone;
        this._nome=nome;
        this._nomeFiador=nomeFiador;
        this._rendaMensal=rendaMensal;
        this._sexo=sexo;
        this._telefone=telefone;

    }
    public set nome(nome: string){
        this._nome = nome
      }
      public get nome(): string{
        return this._nome
     }

     public set telefone(telefone: string){
        this._telefone = telefone
      }
      public get telefone(): string{
        return this._telefone

     } public set fone (fone: string){
   this._fone = fone
     }
     public get fone(): string{
   return this._fone}

    public set dataNascimento(dataNascimento: Date){
      this._dataNascimento = dataNascimento}
    public get dataNascimento(): Date{
       return this._dataNascimento}
 
    public set sexo(sexo: string){
      this._sexo = sexo}
    public get sexo(): string{
      return this._sexo}

      public set email(email: string){
        this._email = email
      }
      public get email(): string{
        return this._email
     }
     public set nomeFiador(nomeFiador: string){
        this._nomeFiador = nomeFiador
      }
      public get nomeFiador(): string{
        return this._nomeFiador
     }
     public set rendaMensal(rendaMensal: number){
        this._rendaMensal = rendaMensal
      }
      public get rendaMensal(): number{
        return this._rendaMensal
     }

}