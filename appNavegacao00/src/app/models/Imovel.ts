export class Imovel {
    private _endereco: string;
    private _bairro: string;
    private _cep: string;
    private _cidade: string;
    private _uf: string;
    private _qtdQuartos: number;
    private _qtdSalas: number;
    private _qtdCozinhas: number;
    private _temGaragem: boolean;
    private _mobiliado: boolean;
    private _valorLocacao: number;
    private _valorVenda: number;
    private _isParaLocacao: boolean;
    private _isVenal: boolean; 
    
    constructor (endereco:string, bairro:string, cep:string, cidade:string, uf:string, qtdQuartos:number, qtdSalas:number, qtdCozinhas:number, temGaragem:boolean, mobiliado:boolean, valorLocacao:number, valorVenda:number, isParaLocacao:boolean, isVenal:boolean)
    {

        this._endereco=endereco;
        this._bairro=bairro;
        this._cep=cep;
        this._cidade = cidade;
        this._isParaLocacao=isParaLocacao;
        this._isVenal=isVenal;
        this._mobiliado=mobiliado;
        this._qtdCozinhas=qtdCozinhas;
        this._qtdQuartos=qtdQuartos;
        this._qtdSalas=qtdSalas;
        this._temGaragem=temGaragem;
        this._uf=uf;
        this._valorVenda=valorVenda;
        this._valorLocacao=valorLocacao;
    
    }
    public set endereco(endereco: string){
        this._endereco = endereco
      }
      public get endereco(): string{
        return this._endereco
     }

     public set bairro(bairro: string){
        this._bairro = bairro
      }
      public get bairro(): string{
        return this._bairro
     }

     public set cep(cep: string){
        this._cep = cep
      }
      public get cep(): string{
        return this._cep
     }

     public set cidade(cidade: string){
        this._cidade = cidade
      }
      public get cidade(): string{
        return this._cidade
     }

     public set isParaLocacao(isParaLocacao: boolean){
        this._isParaLocacao = isParaLocacao
      }
      public get isParaLocacao(): boolean{
        return this._isParaLocacao
     }

     public set isVenal(isVenal: boolean){
        this._isVenal = isVenal
      }
      public get isVenal(): boolean{
        return this._isVenal
     }

     public set mobiliado(mobiliado: boolean){
        this._mobiliado= mobiliado
      }
      public get mobiliado(): boolean{
        return this._mobiliado
     }

     public set qtdCozinhas(qtdCozinhas: number){
        this._qtdCozinhas = qtdCozinhas
      }
      public get qtdCozinhas(): number{
        return this._qtdCozinhas
     }

     public set qtdQuartos(qtdQuartos: number){
        this._qtdQuartos = qtdQuartos
      }
      public get qtdQuartos(): number{
        return this._qtdQuartos
     }

     public set qtdSalas(qtdSalas: number){
        this._qtdSalas = qtdSalas
      }
      public get qtdSalas(): number{
        return this._qtdSalas
     }

     public set temGaragem(temGaragem: boolean){
        this._temGaragem = temGaragem
      }
      public get temGaragem(): boolean{
        return this._temGaragem
     }

     public set uf(uf: String){
        this._uf = uf
      }
      public get uf(): String{
        return this._uf
     }

     public set valorVenda(valorVenda: number){
        this._valorVenda = valorVenda
      }
      public get valorVenda(): number{
        return this._valorVenda
     }

     public set valorLocacao(valorLocacao: number){
        this._valorLocacao = valorLocacao
      }
      public get valorLocacao(): number{
        return this._valorLocacao
     }
}