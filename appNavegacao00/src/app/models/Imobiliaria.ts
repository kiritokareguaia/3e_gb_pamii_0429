export class Imobiliaria {
    private _nome: string;
    private _endereco: string;
    private _cidade: string;
    private _bairro: string;
    private _fone: string;
    private _site: string;

    constructor (nome: string,endereco: string,cidade: string,bairro: string,fone: string,site: string){
        this._nome=nome;
        this._endereco=endereco;
        this._cidade=cidade;
        this._bairro=bairro;
        this._fone=fone;
        this._site=site;

    }
    public set nome(nome: string){
        this._nome = nome
      }
      public get nome(): string{
        return this._nome
     }
     public set endereco(endereco: string){
        this._endereco = endereco
      }
      public get endereco(): string{
        return this._endereco
     }
     public set cidade(cidade: string){
        this._cidade= cidade
      }
      public get cidade(): string{
        return this._cidade
     }
     public set bairro(bairro: string){
        this._bairro = bairro
      }
      public get bairro(): string{
        return this._bairro
     }
     public set fone(_fone: string){
         this.fone=fone
      }
      public get fone(): string{
        return this._fone
     }
     public set site(site: string){
        this._site= site
      }
      public get site(): string{
        return this._site
     }
    
}